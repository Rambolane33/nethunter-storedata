Nexmon is a framework to write firmware patches for the BCM4339, BCM4330, BCM4358 and BCM43438 WiFi chips. This app allows you to install a WiFi firmware that enables monitor mode and frame injection on a rooted Nexus 5 running Android 6.x or 7.x. On other smartphones with Broadcom chips you might activate monitor mode only. Besides the firmware patch, this app installs a couple of penetration testing tools and utilities to interface and configure the Nexmon firmware. In addition, you can do various penetration tests directly from the graphical user interface.

Find out more on https://nexmon.org 

Requires root.

This app is built and signed by Kali NetHunter.
 
