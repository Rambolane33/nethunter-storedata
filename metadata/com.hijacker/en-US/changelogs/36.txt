* Add support for SAR devices while installing the Nexmon firmware
* Made the firmware installation more robust
* Added support for root solutions other than SuperSU
* Initialization process more robust (detect errors, avoid crashes)
* Update version, use new Notification Builder with channel ID, fix min...
* …or code issues (C-style array declarations, use StringBuilder instead of string concatenation)
* Minor fixes, built with updated tools, fix crashes on newer Android versions
* Add optional client count label next to AP tile icons
