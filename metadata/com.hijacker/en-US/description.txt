Hijacker is a Graphical User Interface for the penetration testing tools Aircrack-ng, Airodump-ng, MDK3 and Reaver. It offers a simple and easy UI to use these tools without typing commands in a console and copy&pasting MAC addresses.

This application requires an ARM android device with an internal wireless adapter that supports Monitor Mode. A few android devices do, but none of them natively. This means that you will need a custom firmware. Any device that uses the BCM4339 chipset (MSM8974, such as Nexus 5, Xperia Z1/Z2, LG G2, LG G Flex, Samsung Galaxy Note 3) will work with Nexmon (which also supports some other chipsets). Devices that use BCM4330 can use bcmon.

An alternative would be to use an external adapter that supports monitor mode in Android with an OTG cable.

The required tools are included for armv7l and aarch64 devices as of version 1.1. The Nexmon driver and management utility for BCM4339 and BCM4358 are also included.

Root access is also necessary, as these tools need root to work.
 
