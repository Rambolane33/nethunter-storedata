Are you missing the key layout you're used to from your computer? This keyboard has separate number keys, punctuation in the usual places, and arrow keys. It is based on the AOSP Gingerbread soft keyboard, so it supports multitouch for the modifier keys.

This keyboard is especially useful if you use ConnectBot for SSH access. It provides working Tab/Ctrl/Esc keys, and the arrow keys are essential for devices that don't have a trackball or D-Pad.

HAVE FEEDBACK, QUESTIONS, OR BUG REPORTS? Please check https://code.google.com/p/hackerskeyboard/wiki/FrequentlyAskedQuestions or email me. It's difficult to track issues reported through review comments.

Completion dictionaries are provided by plug-in packages, see the "other applications by this developer" section for the currently available list. The keyboard also supports use of dictionaries (but not keyboard layouts) provided by AnySoftKeyboard language packs.

For more information, please see the documentation:
https://code.google.com/p/hackerskeyboard/wiki/UsersGuide
https://code.google.com/p/hackerskeyboard/wiki/FrequentlyAskedQuestions
https://code.google.com/p/hackerskeyboard/wiki/ReleaseNotes
https://code.google.com/p/hackerskeyboard/

In case an updated version is not working for you, you can download older releases here:
https://code.google.com/p/hackerskeyboard/downloads/list?q=label:Release&sort=-filename

Known issues include:

- Localization for the keyboard layout is incomplete, currently the Spanish, Finnish, Lao, Romanian, and Thai are only localized in 4-row mode and show the English QWERTY layout in 5-row mode.

- Some languages and layouts may not be usable on your phone if it lacks the necessary fonts.

- Right-to-left languages (Arabic, Hebrew) are apparently not fully supported on pre-Honeycomb devices. Your mileage may vary.

- Many applications don't react to additional keys since they aren't programmed to handle them.

The supported keyboard layouts include:
- Armenian (Հայերեն)
- Arabic (العربية)
- Bulgarian (български език)
- Czech (Čeština)
- Danish (dansk)
- English Dvorak (language "en_DV")
- English (QWERTY)
- English/British (en_GB)
- Finnish (Suomi)
- French (Français, AZERTY)
- German (Deutsch, QWERTZ)
- Greek (ελληνικά)
- Hebrew (עברית)
- Hungarian (Magyar)
- Italian (Italiano)
- Lao (ພາສາລາວ)
- Norwegian (Norsk bokmål)
- Persian (فارسی)
- Portugese (Português)
- Romanian (Română)
- Russian (Русский)
- Russian phonetic (Русский, ru-rPH)
- Serbian (Српски)
- Slovak (Slovenčina)
- Slovenian (Slovenščina)/Bosnian/Croatian/Latin Serbian
- Spanish (Español, Español Latinoamérica)
- Swedish (Svenska)
- Tamil (தமிழ்)
- Thai (ไทย)
- Turkish (Türkçe)
- Ukrainian (українська мова)
