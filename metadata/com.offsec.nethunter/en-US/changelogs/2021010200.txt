2021.1
------
* Update build info and boot messages    - @re4son
* Add module search directories          - @Martinvlba
* Add Glitch boot animation              - @s133py

2020.4
------
* Add settings menu                      - @yesimxev
* Add boot animaton selector             - @yesimxev
* Add config import/export function      - @yesimxev
* Improvements to Wardriving / GPS menu  - @DraCode
* Updates to DuckHunter                  - @TheMMcOfficial
* Updates to hostapd                     - @Martinvlba