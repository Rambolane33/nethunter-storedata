* Bookmark frequencies
* Including hackrf_android 1.12 (support for rad1o)
* Select unit for frequency and bandwidth (MHz,kHz,Hz) in jump dialog
* Support for Marshmallow permission model
* Many bugfixes
